﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml.Linq;

namespace SentePiramida
{
    class XmlPyramid
    {
        public PyramidMember CreatePyramidStructureFromFile(string filename)
        {
            XDocument XDoc = XDocument.Load(filename);
            var result = XDoc.Descendants("uczestnik");
            PyramidMember root = new PyramidMember(Int32.Parse(result.First().Attribute("id").Value));

            PostOrder(result.First(), root);

            return root;
        }

        public void ReadTransfersFromXMLInToPyramid(PyramidMember root, string transfersFileName)
        {
            var XDocTransfers = XDocument.Load(transfersFileName);
            var transfers = XDocTransfers.Descendants("przelewy").Elements();
            ReadTransfersByRecursion(root, transfers);
        }

        private void ReadTransfersByRecursion(PyramidMember root, IEnumerable<XElement> doc)
        {
            foreach (var element in root.children)
            {
                element.SetTransfersSumFromXML(doc);
                ReadTransfersByRecursion(element, doc);
            }
        }

        private void PostOrder(XElement element, PyramidMember currentMember)
        {
            foreach (var x in element.Elements())
            {
                PyramidMember childMember = currentMember.AddChild(Int32.Parse(x.Attribute("id").Value));
                PostOrder(x, childMember);
            }
        }

        static public bool CheckIfFilesExists(string path, string transfersFileName, string pyramidFileName)
        {
            if (!File.Exists(path + transfersFileName) || !File.Exists(path + pyramidFileName))
                return false;
            else
                return true;
        }
    }
}
