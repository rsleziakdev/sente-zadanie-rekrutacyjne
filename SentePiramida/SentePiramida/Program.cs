﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Xml.Linq;

namespace SentePiramida
{
    class Program
    {
        static void Main(string[] args)
        {
            var path = AppDomain.CurrentDomain.BaseDirectory;
            var pyramidFileName = "piramida.xml";
            var transfersFileName = "przelewy.xml";

            if (!XmlPyramid.CheckIfFilesExists(path, transfersFileName, pyramidFileName))
            {
                Console.WriteLine("Nie znaleziono wymaganych plików xml!");
                Console.WriteLine("Nazwy wymaganych plików to: " + transfersFileName + " i " + pyramidFileName);
                Console.WriteLine("Pliki muszą znajdować się w folderze z aplikacją - ");
                Console.WriteLine(path);
                Console.ReadKey();
                return;
            }

            var XMLPyramid = new XmlPyramid();
            var pyramidRoot = XMLPyramid.CreatePyramidStructureFromFile(path + pyramidFileName);
            XMLPyramid.ReadTransfersFromXMLInToPyramid(pyramidRoot, path + transfersFileName);         

            var pyramid = new List<PyramidMember>();
            pyramidRoot.DescendantsNodesToList(pyramid);
            pyramid.Add(pyramidRoot);
            pyramidRoot.DisplayTreeInfo(pyramid);


                  
        }
    }
}
