﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;

namespace SentePiramida
{
    class PyramidMember
    {
        public int id { get; private set; }

        private PyramidMember parent;

        private int transferedAmmount;

        public List<PyramidMember> children { get; set; }

        public PyramidMember(int id)
        {
            this.id = id;
            this.children = new List<PyramidMember>();
            this.transferedAmmount = 0;
        }

        public int Level
        {
            get
            {
                if (IsRoot())
                    return 0;
                return parent.Level + 1;
            }
        }

        public bool IsRoot()
        {
            return parent == null;
        }

        public PyramidMember AddChild(int id)
        {
            var childNode = new PyramidMember(id)
            {
                parent = this
            };
            children.Add(childNode);

            return childNode;
        }

        public int ChildsAmmountWithoutDescendants(int result)
        {
            foreach (var child in children)
            {
                if (child.children.Count == 0)
                    result++;
                else
                    result = child.ChildsAmmountWithoutDescendants(result);
            }
            return result;
        }

        public int SetTransfersSumFromXML(IEnumerable<XElement> elements)
        {
            var transfers = elements
                .Where(a => Int32.Parse(a.Attribute("od").Value) == id)
                .Sum(x => Int32.Parse(x.Attribute("kwota").Value));

            this.transferedAmmount = transfers;
            return transferedAmmount;
        } 

        public int CalculateTrasnfersSumFromChildren(int childrenTransfersSum)
        {
            foreach (var member in children)
            {
                childrenTransfersSum += member.transferedAmmount + member.CalculateTrasnfersSumFromChildren(0);
            }
            return childrenTransfersSum;
        }

        public double CalculateObtainedProvision()
        {
            double provision = 0;

            if (!IsRoot())
            {
                foreach(var child in children)
                {
                    provision += Math.Ceiling(child.transferedAmmount / Math.Pow(2, this.Level));
                    provision += child.CalculateTrasnfersSumFromChildren(0) / Math.Pow(2, child.Level);
                }
            }
            else
                provision = CalculateObtainedProvisionForRootNode();

            return Math.Floor(provision);
        }

        private double CalculateObtainedProvisionForRootNode()
        {
            double provision = 0;
            foreach (var directChild in children)
            {
                provision += directChild.transferedAmmount;
                provision += directChild.CalculateTrasnfersSumFromChildren(0) / Math.Pow(2, 1);
            }
            return provision;
        }

        public void DescendantsNodesToList(List<PyramidMember> pyramindMembers)
        {
            foreach (var member in this.children)
            {
                pyramindMembers.Add(member);
                member.DescendantsNodesToList(pyramindMembers);
            }
        }



        public void DisplayTreeInfo(List<PyramidMember> pyramindMembers)
        {
            pyramindMembers = pyramindMembers.OrderBy(a => a.id).ToList();

            foreach (var member in pyramindMembers)
            {
                Console.Write(member.id);
                Console.Write(" " + member.Level);
                Console.Write(" " + member.ChildsAmmountWithoutDescendants(0));
                Console.WriteLine(" " + member.CalculateObtainedProvision());

            }

        }

        private void DisplayChildsInfoByRecursion() //Not used
        {
            Console.Write(id);
            Console.Write(" " + Level);
            Console.Write(" " + ChildsAmmountWithoutDescendants(0));
            Console.WriteLine(" " + CalculateObtainedProvision());

            foreach (var child in children)
                child.DisplayChildsInfoByRecursion();
        }
      
    }
}
